//
//  SelectLanguageVC.swift
//  RedUzTeacher
//
//  Created by Kuziboev Siddikjon on 11/18/21.
//

import UIKit

class SelectLanguageVC: UIViewController {
 
    
  
    //Third View
    
    @IBOutlet weak var otpView: OTPFieldView!{
        didSet {

        }
    }
    
    
    //Second Page
    @IBOutlet weak var lblVerifyPhoneNumber: UILabel!
    
    @IBOutlet weak var textFieldContainerView: UIView! {
        didSet {
            textFieldContainerView.addShadow(offset: CGSize(width: 0, height: 0), color: #colorLiteral(red: 0.2392156863, green: 0.4078431373, blue: 1, alpha: 0.08), radius: 4, opacity: 1)

            textFieldContainerView.layer.cornerRadius = 24
            
            

        }
    }
    
    
    @IBOutlet weak var phoneTF: UITextField!{
        didSet{
            phoneTF.delegate = self
            phoneTF.returnKeyType = .done
            phoneTF.keyboardType = .phonePad
        }
    }
    
  
    //First Page

    @IBOutlet weak var backBtn: UIButton!
    
    
    @IBOutlet weak var pageControll: FilledPageControl!
    
    @IBOutlet weak var openPageScrollView: UIScrollView!
    
    @IBOutlet weak var ruConatainerView: UIView!{
        didSet {
            ruConatainerView.addShadow(offset: CGSize(width: 0, height: 0), color: #colorLiteral(red: 0.2392156863, green: 0.4078431373, blue: 1, alpha: 0.08), radius: 4, opacity: 1)
     
            ruConatainerView.layer.cornerRadius = 24
        }
    }
    
    
    @IBOutlet weak var enConatainerView: UIView!{
        didSet {
            enConatainerView.addShadow(offset: CGSize(width: 0, height: 0), color: #colorLiteral(red: 0.2392156863, green: 0.4078431373, blue: 1, alpha: 0.08), radius: 4, opacity: 1)
     
            enConatainerView.layer.cornerRadius = 24
        }
    }
    
    
    @IBOutlet weak var uzbBtnContainerView: UIView!{
        didSet {
            uzbBtnContainerView.addShadow(offset: CGSize(width: 0, height: 0), color: #colorLiteral(red: 0.2392156863, green: 0.4078431373, blue: 1, alpha: 0.08), radius: 4, opacity: 1)
     
            uzbBtnContainerView.layer.cornerRadius = 24
        }
    }
    
    @IBOutlet weak var nextBtn: UIButton! {
        didSet {
            nextBtn.addShadow(offset: CGSize(width: 3, height: 3), color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.12), radius: 4, opacity: 1)
            nextBtn.layer.cornerRadius = 24
            nextBtn.layer.masksToBounds = false

        }
    }
    
    @IBOutlet weak var enBtn: UIButton!
    
    @IBOutlet weak var ruBtn: UIButton!
    
    @IBOutlet weak var uzBtn: UIButton!
    
    @IBOutlet weak var lblSelectLanguage: UILabel!
  
    var count = 0
    
    var otp = ""
    
    var otpIsFilled: Bool = false
    
    var phoneNumber = "+998"
    
    //MARK: viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backBtn.isHidden = count == 0
        nextBtn.backgroundColor = #colorLiteral(red: 0.1755591035, green: 0.4132065177, blue: 1, alpha: 0.599777929)
        nextBtn.isEnabled = false
        keyboardHide()
        setupOtpView()
    }

    override func viewDidAppear(_ animated: Bool) {
       
    }
    
    //MARK: Actions
    @IBAction func nextBtnPressed(_ sender: Any) {
               
        let vc = MainTabbarC()
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
        if UserDefaults.standard.string(forKey: Keys.LANGUAGE) != "nil" {
            
            print(count,"f")
            if count < 2 {
                count += 1

                pageControll.progress = CGFloat(count)
                openPageScrollView.setContentOffset(CGPoint(x: Int(UIScreen.main.bounds.width)*(count), y: 0), animated: true)
                nextBtn.isEnabled = false
                nextBtn.backgroundColor = #colorLiteral(red: 0.1755591035, green: 0.4132065177, blue: 1, alpha: 0.599777929)

                backBtn.isHidden = count == 0

            }
            
            if count == 2  {
                setupOtpView()
            }
            
            if count == 2 && otpIsFilled  {
            /////let vc = MainTabbarC()
//                vc.modalPresentationStyle = .fullScreen
//                present(vc, animated: true, completion: nil)
            }
                
                if count == 1 {
                    if phoneTF.text!.count == 17 {
                        nextBtn.isEnabled = true
                        nextBtn.backgroundColor = #colorLiteral(red: 0.3017732799, green: 0.509286046, blue: 1, alpha: 1)
                    }else {
                        nextBtn.isEnabled = false
                        nextBtn.backgroundColor = #colorLiteral(red: 0.1755591035, green: 0.4132065177, blue: 1, alpha: 0.599777929)
                    }
                }
            
            
            
            
        }
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        
        
        if count >= 1 {
            count -= 1
            nextBtn.isEnabled = false
            nextBtn.backgroundColor = #colorLiteral(red: 0.1755591035, green: 0.4132065177, blue: 1, alpha: 0.599777929)

            backBtn.isHidden = count == 0
            pageControll.progress = CGFloat(count)
            openPageScrollView.setContentOffset(CGPoint(x: Int(UIScreen.main.bounds.width)*(count), y: 0), animated: true)
        }
        
        if UserDefaults.standard.string(forKey: Keys.LANGUAGE) != "nil" {
            nextBtn.backgroundColor = #colorLiteral(red: 0.3017732799, green: 0.509286046, blue: 1, alpha: 1)
            nextBtn.isEnabled = count == 0
            
        }
        
        if count == 1 {
            otpIsFilled = false
            if phoneTF.text!.count == 17 {
                nextBtn.isEnabled = true
                nextBtn.backgroundColor = #colorLiteral(red: 0.3017732799, green: 0.509286046, blue: 1, alpha: 1)
            }else {
                nextBtn.isEnabled = false
                nextBtn.backgroundColor = #colorLiteral(red: 0.1755591035, green: 0.4132065177, blue: 1, alpha: 0.599777929)
            }
        }
       
       
    }
    
    //UIButton Tags-> 1,2,3
    @IBAction func languageBtnTapped(_ sender: UIButton) {
        
        
        clearAllBorderLanguageBts()
        
        nextBtn.backgroundColor = #colorLiteral(red: 0.3017732799, green: 0.509286046, blue: 1, alpha: 1)
        nextBtn.isEnabled = true

        switch sender.tag {
            
        case 1:
            
            Cache.saveLanguage(language: "uz")
            uzbBtnContainerView.layer.borderColor = #colorLiteral(red: 0.3017732799, green: 0.509286046, blue: 1, alpha: 1)
            uzbBtnContainerView.layer.borderWidth = 1
        case 2:
            
            Cache.saveLanguage(language: "ru")
            ruConatainerView.layer.borderColor = #colorLiteral(red: 0.3017732799, green: 0.509286046, blue: 1, alpha: 1)
            ruConatainerView.layer.borderWidth = 1
        default:
            
            Cache.saveLanguage(language: "en")
            enConatainerView.layer.borderColor = #colorLiteral(red: 0.3017732799, green: 0.509286046, blue: 1, alpha: 1)
            enConatainerView.layer.borderWidth = 1
        }
        
       
    }
    
 
    
    
    
    
}
//MARK: Birnimalar
extension SelectLanguageVC: UIScrollViewDelegate {
    
    func clearAllBorderLanguageBts() {
        uzbBtnContainerView.layer.borderColor = UIColor.white.cgColor
        uzbBtnContainerView.layer.borderWidth = 1
        
        ruConatainerView.layer.borderColor = UIColor.white.cgColor
        ruConatainerView.layer.borderWidth = 1
        
        enConatainerView.layer.borderColor = UIColor.white.cgColor
        enConatainerView.layer.borderWidth = 1
    }
   
    func scroll() {
        openPageScrollView.delegate = self
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
    private func keyboardHide(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tapGesture)
        
    }
    
    @objc func dismissKeyboard(){
        self.view.endEditing(true)
    }
}

//MARK: PhoneTF

extension SelectLanguageVC: UITextFieldDelegate{
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if phoneTF.text?.count == 17 {
            phoneTF.resignFirstResponder()
            nextBtn.isEnabled = true
            nextBtn.backgroundColor = #colorLiteral(red: 0.3017732799, green: 0.509286046, blue: 1, alpha: 1)
        }else {
            nextBtn.isEnabled = false
            nextBtn.backgroundColor = #colorLiteral(red: 0.1755591035, green: 0.4132065177, blue: 1, alpha: 0.599777929)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        phoneTF.text = phoneNumber
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        phoneNumber = phoneTF.text!
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        let newString = (text as NSString).replacingCharacters(in: range, with: string)
        textField.text?.formatPhoneNumber(with: "+XXX XX XXX XX XX", phone: newString)
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        phoneTF.resignFirstResponder()
        return true
    
    }
    
}


//MARK: OTP
extension SelectLanguageVC: OTPFieldViewDelegate {
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp: String) {
        self.otp = otp
    }
    
    func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
        nextBtn.isEnabled = hasEnteredAll
        self.otpIsFilled = hasEnteredAll
        nextBtn.backgroundColor = hasEnteredAll ? #colorLiteral(red: 0.3017732799, green: 0.509286046, blue: 1, alpha: 1) : #colorLiteral(red: 0.1755591035, green: 0.4132065177, blue: 1, alpha: 0.599777929)
        
        return false
    }
    
    
    private func setupOtpView(){
        
        self.otpView.fieldsCount = 4
        self.otpView.fieldBorderWidth = 1

        self.otpView.defaultBorderColor = #colorLiteral(red: 0.3017732799, green: 0.509286046, blue: 1, alpha: 0.08)
        self.otpView.filledBorderColor = #colorLiteral(red: 0.3017732799, green: 0.509286046, blue: 1, alpha: 1)
        self.otpView.cursorColor = UIColor.black
        self.otpView.displayType = .roundedCorner
        if isSmallScreen{
            self.otpView.fieldSize = 40
        }else{
            self.otpView.fieldSize = 45
        }
        self.otpView.separatorSpace = 20
        self.otpView.shouldAllowIntermediateEditing = false
        self.otpView.delegate = self
        self.otpView.initializeUI()
    }
    
}
