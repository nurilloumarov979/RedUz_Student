//
//  AppDelegate.swift
//  RedUz_Student
//
//  Created by apple on 07/12/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow()
        let vc = SplashVC(nibName: "SplashVC", bundle: nil)
        vc.modalPresentationStyle = .automatic
        window?.rootViewController = vc
        window?.makeKeyAndVisible()
        return true
    }



}

