//
//  MainTab.swift
//  RedUz_Student
//
//  Created by apple on 09/12/21.
//

import UIKit

class MainTab: UITabBarController {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let vc1 = HomeVC(nibName: "HomeVC", bundle: nil)
        vc1.tabBarItem.image = UIImage(named: Icons.home)
        let nav1 = UINavigationController(rootViewController: vc1)
        
        let vc2 = SettingVC(nibName: "SettingVC", bundle: nil)
        vc2.tabBarItem.image = UIImage(named: Icons.setting)
        let nav2 = UINavigationController(rootViewController: vc2)
        self.viewControllers = [nav1, nav2]

        self.tabBar.unselectedItemTintColor = #colorLiteral(red: 0.6470588235, green: 0.6470588235, blue: 0.6470588235, alpha: 1)
        self.tabBar.layer.shadowColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        self.tabBar.layer.shadowOffset = CGSize(width: 0,height: -2)
        self.tabBar.layer.shadowRadius = 3
        self.tabBar.layer.shadowOpacity = 0.3
        self.tabBar.backgroundColor = .white
        //Remove UITabbar upper border line
        if #available(iOS 13, *) {
            // iOS 13:
            let appearance = tabBar.standardAppearance
            appearance.configureWithOpaqueBackground()
            appearance.shadowImage = nil
            appearance.shadowColor = nil
            tabBar.standardAppearance = appearance
            
        } else {
            // iOS 12 and below:
            tabBar.shadowImage = UIImage()
            tabBar.backgroundImage = UIImage()
        }

    }
   
    
    
}
