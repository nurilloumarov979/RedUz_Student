//  Colors.swift
//  RedUz_Student
//  Created by apple on 08/12/21.

import Foundation
import UIKit

struct Colors{
    static var customBlue = #colorLiteral(red: 0.2392156863, green: 0.4078431373, blue: 1, alpha: 1)
    static var customBlue2 = #colorLiteral(red: 0.2392156863, green: 0.4078431373, blue: 1, alpha: 0.6)
    static var customGreen = #colorLiteral(red: 0.2392156863, green: 0.4078431373, blue: 1, alpha: 1)
    static var customGreen2 = #colorLiteral(red: 0.2392156863, green: 0.4078431373, blue: 1, alpha: 1)
    static var customGray = #colorLiteral(red: 0.2392156863, green: 0.4078431373, blue: 1, alpha: 1)
    static var customRed = #colorLiteral(red: 0.9882352941, green: 0.02745098039, blue: 0.02745098039, alpha: 1)
    static var customRed2 = #colorLiteral(red: 0.9960784314, green: 0.231372549, blue: 0.1882352941, alpha: 1)
    static var customYellow = #colorLiteral(red: 0.2392156863, green: 0.4078431373, blue: 1, alpha: 1)
    static var customBackgraund = #colorLiteral(red: 0.9529411765, green: 0.9647058824, blue: 1, alpha: 1)
}


