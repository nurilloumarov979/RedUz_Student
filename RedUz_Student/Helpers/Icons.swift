
//  Icons.swift
//  RedUz_Student
//  Created by apple on 08/12/21.

import Foundation

struct Icons{

    static var deSelectHome = "de_select_home"
    static var home = "home"
    static var calendar_month_grey = "calendar_month_grey"
    static var calendar_month = "calendar_month"
    static var chevron_down = "chevron_down"
    static var chevron_left = "chevron_left"
    static var chevron_right = "chevron_right"
    static var chevron_up = "chevron_up"
    static var bell = "bell"
    static var flag_english = "flag_english"
    static var flag_russian = "flag_russian"
    static var flag_uzbek = "flag_uzbek"
    static var trash = "trash"
    static var person = "person"
    static var phone = "phone"
    static var message = "message"
    static var xVector = "xVector"
    static var history_grey = "history_grey"
    static var plus = "plus"
    static var setting = "setting"
    static var history_white = "history_white"
    static var ring = "ring"
    static var ringFull = "ringFull"
    
}
