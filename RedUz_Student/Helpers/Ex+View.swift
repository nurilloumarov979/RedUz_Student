//
//  Ex+View.swift
//  RedUz_Student
//
//  Created by apple on 09/12/21.
//

import Foundation
import UIKit


extension UIView {
    func addShadow(offset: CGSize, color: CGColor, radius: CGFloat, opacity: Float){
        self.layer.shadowOffset = offset
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = radius
        self.layer.shadowColor = color
    }
}

