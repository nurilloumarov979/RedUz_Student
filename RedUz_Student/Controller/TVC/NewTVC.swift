//
//  NewTVC.swift
//  RedUz_Student
//
//  Created by apple on 13/12/21.
//

import UIKit

class NewTVC: UITableViewCell {

    @IBOutlet weak var newsImg: UIImageView!
    
    @IBOutlet weak var dateLbl: UILabel!
    
    @IBOutlet weak var informationLbl: UILabel!
    
    static let identifier = "NewTVC"
    static func nib()->UINib{return UINib(nibName: identifier, bundle: nil)}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
}
