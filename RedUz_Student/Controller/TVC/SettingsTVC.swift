//
//  SettingsTVC.swift
//  RedUzTeacher
//
//  Created by Kuziboev Siddikjon on 12/2/21.
//

import UIKit


class SettingsTVC: UITableViewCell {
    
    static let identifier = "SettingsTVC"
    static func nib()-> UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblBranchName: UILabel!
    
    @IBOutlet weak var bottomLineView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func updateCell(index: IndexPath, data: [DataDM3]) {
        
        bottomLineView.isHidden = index.row + 1 == data[index.section].rowTitle.count
                
        lblBranchName.isHidden = !(index.row == 0 && index.section == 0)
        lblTitle.text = data[index.section].rowTitle[index.row]
        
    }
    
    func updateCellForSelectBranchVC(index: IndexPath, data: [DataDM3])  {
        bottomLineView.isHidden = index.row + 1 == data[0].rowTitle.count
        lblBranchName.isHidden = true
        lblTitle.text = data[0].rowTitle[index.row]
    }

}
