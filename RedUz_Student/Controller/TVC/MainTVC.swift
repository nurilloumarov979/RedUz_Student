//
//  MainTVC.swift
//  RedUzTeacher
//
//  Created by Kuziboev Siddikjon on 11/19/21.
//

import UIKit

class MainTVC: UITableViewCell {

    static let identifier = "MainTVC"
    static func nib()-> UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    @IBOutlet weak var containerView: UIView!{
        didSet {
            containerView.addShadow(offset: CGSize(width: 0, height: 0), color: #colorLiteral(red: 0.3017732799, green: 0.509286046, blue: 1, alpha: 0.12), radius: 4, opacity: 1)        }
    }
    @IBOutlet weak var lblRoomName: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var lblFISH: UILabel!
    
    @IBOutlet weak var lblLessonType: UILabel!
    
    @IBOutlet weak var lblVCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func updateCell(index: IndexPath) {
        
        
//        #colorLiteral(red: 0.3017732799, green: 0.509286046, blue: 1, alpha: 0.12)
        
        
        if index.row == 0 {
            containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            containerView.layer.cornerRadius = 12
        }
        
        if index.row == 4 {
            containerView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            containerView.layer.cornerRadius = 12
           
        }
        
      
    }
}
