//
//  AllGiftPayTVC.swift
//  RedUz_Student
//
//  Created by apple on 13/12/21.
//

import UIKit

class AllGiftPayTVC: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var paymentLBL: UILabel!
    static let identifier = "AllGiftPayTVC"
    static func nib()->UINib{return UINib(nibName: identifier, bundle: nil)}
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
    func updateCell(index: IndexPath) {
        
        
//        #colorLiteral(red: 0.3017732799, green: 0.509286046, blue: 1, alpha: 0.12)

        if index.row == 0 {
            containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            containerView.layer.cornerRadius = 12
        }
        
        if index.row == 4 {
            containerView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            containerView.layer.cornerRadius = 12
        }
    }

    
}
