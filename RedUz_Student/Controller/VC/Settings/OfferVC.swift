
//  OfferVC.swift
//  RedUz_Student
//  Created by apple on 14/12/21.

import UIKit

class OfferVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpNavigation()
    }
    
    
    @IBAction func offerPressed(_ sender: Any) {
    }
    
}


//MARK: NAVIGATION ITEMS
extension OfferVC{
    func setUpNavigation(){
        title = "Taklif va Shikoyatlar"
        let backButton = UIBarButtonItem()
        backButton.title = ""
        backButton.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        self.navigationController?.navigationBar.layer.cornerRadius = 12
        self.navigationController?.navigationBar.clipsToBounds = true
        self.navigationController?.navigationBar.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        
        navigationController?.navigationBar.update(backroundColor: Colors.customBlue, titleColor: .white)
    }
}
