//
//  ViewController.swift
//  RedUzTeacher
//
//  Created by Kuziboev Siddikjon on 12/2/21.
//

import UIKit

struct DataDM3 {
    var sectionTitle: String
    var rowTitle: [String]
}


class SelectBranchAlertVC: UIViewController {
    @IBOutlet weak var containerHeight: NSLayoutConstraint!
    
    @IBOutlet weak var containerView: UIView!{
        didSet {
            containerView.layer.maskedCorners =  [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            containerView.layer.cornerRadius = 15
            
        }
    }
    @IBOutlet weak var tableView: UITableView!
    
    var pointOrigin: CGPoint?
    
    var hasSetPointOrigin = false
        
    var data : [DataDM3] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpTableView()
        
        data.append(DataDM3(sectionTitle: "", rowTitle: ["Shirin Filiali", "Shirin Filiali", "Shirin Filiali", "Shirin Filiali","Shirin Filiali","Shirin Filiali","Shirin Filiali"]))
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerAction))
        
        containerView.addGestureRecognizer(panGesture)
    }
    
    override func viewDidLayoutSubviews() {
        if !hasSetPointOrigin {
            hasSetPointOrigin = true
            pointOrigin = self.view.frame.origin
        }
        
    }
    @IBAction func dissmissBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func panGestureRecognizerAction(sender: UIPanGestureRecognizer) {
        
        let translation = sender.translation(in: view)
        
        // Not allowing the user to drag the view upward
        guard translation.y >= 0 else { return }
        
        //             setting x as 0 because we don't want users to move the frame side ways!! Only want straight up or down
        
        view.frame.origin = CGPoint(x: 0, y: self.pointOrigin!.y + translation.y)
        
        if sender.state == .ended {
            let dragVelocity = sender.velocity(in: view)
            if dragVelocity.y >= 1000 {
                self.dismiss(animated: true, completion: nil)
            } else {
                // Set back to original position of the view controller
                UIView.animate(withDuration: 0.3) {
                    self.view.frame.origin = self.pointOrigin ?? CGPoint(x: 0, y: 200)
                }
            }
        }
    }
    
    
}




//MARK: UITableView
extension SelectBranchAlertVC: UITableViewDataSource, UITableViewDelegate {
    
    func setUpTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(SettingsTVC.nib(), forCellReuseIdentifier: SettingsTVC.identifier)
        tableView.separatorStyle = .none
    }
    
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        containerHeight.constant = CGFloat(data[0].rowTitle.count*50+50)
        return data[0].rowTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SettingsTVC.identifier, for: indexPath)as! SettingsTVC
        cell.updateCellForSelectBranchVC(index: indexPath, data: data)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true, completion: nil)
    }
    
}


