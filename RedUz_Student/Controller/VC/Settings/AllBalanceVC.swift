//
//  AllBalanceVC.swift
//  RedUz_Student
//
//  Created by apple on 13/12/21.
//

import UIKit

class AllBalanceVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
            tableView.register(AllGiftPayTVC.nib(), forCellReuseIdentifier: AllGiftPayTVC.identifier)
            tableView.separatorStyle = .none
            tableView.contentInset  = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)

        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpNavigation()
    }


}



//MARK: TABLE VIEW
extension AllBalanceVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AllGiftPayTVC.identifier, for: indexPath) as! AllGiftPayTVC
       // cell.updateCell(index: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let v = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width-48, height: 28))
        v.backgroundColor = .clear
        
        
        let v2 = UIView(frame: CGRect(x: 8, y: 0, width: v.frame.width-16, height: 28))
        v2.backgroundColor = .white
        v2.layer.cornerRadius = 12
        v2.addShadow(offset: CGSize(width: 2, height: 2), color: #colorLiteral(red: 0.3017732799, green: 0.509286046, blue: 1, alpha: 0.12), radius: 4, opacity: 1)
        v.addSubview(v2)
        
        //Label 1
        let dateLabel = UILabel(frame: CGRect(x: 12, y: 14, width: 100, height: 25))
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.text = "16-Iyun"
        dateLabel.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        dateLabel.textColor =  #colorLiteral(red: 0.1921568627, green: 0.2117647059, blue: 0.2862745098, alpha: 1)
        dateLabel.textAlignment = .left
        v2.addSubview(dateLabel)
        dateLabel.leftAnchor.constraint(equalTo: v2.leftAnchor, constant: 12).isActive = true
        dateLabel.centerYAnchor.constraint(equalTo: v2.centerYAnchor, constant: 0).isActive = true

        return v
    }

    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        34
    }

}

//MARK: NAVIGATION ITEMS
extension AllBalanceVC{
    
    func setUpNavigation(){
        title = "Balans tarixi"
        let backButton = UIBarButtonItem()
        backButton.title = ""
        backButton.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        self.navigationController?.navigationBar.layer.cornerRadius = 12
        self.navigationController?.navigationBar.clipsToBounds = true
        self.navigationController?.navigationBar.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        navigationController?.navigationBar.update(backroundColor: Colors.customBlue, titleColor: .white)
    }
    
}

