//
//  OfferAndSuggestionsVC.swift
//  RedUzTeacher
//
//  Created by Kuziboev Siddikjon on 12/13/21.
//

import UIKit

class OfferAndSuggestionsVC: UIViewController {
    
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    @IBOutlet weak var sendBtn: UIButton!{
        didSet {
            sendBtn.addShadow(offset: CGSize(width: 0, height: 0), color: #colorLiteral(red: 0.2392156863, green: 0.4078431373, blue: 1, alpha: 0.12), radius: 4, opacity: 0.9)
        }
    }
    
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var lblQuestion: UILabel!
    
    var theme: String = "Mavzu: "
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpSegmentControl()
        setUpTextView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpNavigation()
    }
    
    @IBAction func segmentControlTapped(_ sender: UISegmentedControl) {
        
        switch segmentControl.selectedSegmentIndex {
        case 0:
            print("taklif")
        default:
            print("Shikoyat")
            
        }
        
    }
    
    @IBAction func sendBtnPressed(_ sender: Any) {
    }
    
    func setUpSegmentControl() {
        segmentControl.setTitle("Taklif", forSegmentAt: 0)
        segmentControl.setTitle("Shikoyat", forSegmentAt: 1)
        //        segmentControl.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 18, weight: .semibold) ], for: .normal)
        segmentControl.tintColor = .black
        segmentControl.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9490196078, blue: 0.9647058824, alpha: 1)
        segmentControl.addShadow(offset: CGSize(width: 2, height: 2), color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.03270039337), radius: 2, opacity: 1)
        segmentControl.tintColor = .red
    }
    
}



//MARK: TextViewDelegate
extension OfferAndSuggestionsVC: UITextViewDelegate {
   
    func setUpTextView() {
        textView.delegate = self
        textView.text = theme
        textView.textColor = UIColor.lightGray
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = theme
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        if textView.text.isEmpty || textView.text.count < theme.count {
            textView.text = theme
            textView.textColor = UIColor.lightGray
        }else {
            textView.textColor = UIColor.black

        }
    }
}


//MARK: NAVIGATION ITEMS
extension OfferAndSuggestionsVC{
    
    func setUpNavigation(){
        title = "Yangiliklar"
        let backButton = UIBarButtonItem()
        backButton.title = ""
        backButton.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        self.navigationController?.navigationBar.layer.cornerRadius = 12
        self.navigationController?.navigationBar.clipsToBounds = true
        self.navigationController?.navigationBar.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        navigationController?.navigationBar.update(backroundColor: Colors.customBlue, titleColor: .white)
    }
    
}


