//
//  HistoryBalanceVC.swift
//  RedUz_Student
//
//  Created by apple on 11/12/21.
//

import UIKit

class HistoryBalanceVC: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpNavigation()
    }

    @IBAction func allBalancePressed(_ sender: Any) {
        let vc = AllBalanceVC(nibName: "AllBalanceVC", bundle: nil)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func paymentPressed(_ sender: Any) {
        let vc = PaymentVC(nibName: "PaymentVC", bundle: nil)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func giftPressed(_ sender: Any) {
        let vc = GiftVC(nibName: "GiftVC", bundle: nil)
        navigationController?.pushViewController(vc, animated: true)
    }

}


//MARK: NAVIGATION ITEMS
extension HistoryBalanceVC{
    
    func setUpNavigation(){
        title = "Balans tarixi"
        let backButton = UIBarButtonItem()
        backButton.title = ""
        backButton.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        self.navigationController?.navigationBar.layer.cornerRadius = 12
        self.navigationController?.navigationBar.clipsToBounds = true
        self.navigationController?.navigationBar.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        navigationController?.navigationBar.update(backroundColor: Colors.customBlue, titleColor: .white)
    }
    
}

