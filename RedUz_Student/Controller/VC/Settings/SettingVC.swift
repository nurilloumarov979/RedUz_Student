

//  SettingVC.swift
//  RedUz_Student
//  Created by apple on 09/12/21.

import UIKit

class SettingVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpNavigation()
    }
    
    @IBAction func balansBtnPressed(_ sender: Any) {
        let vc = HistoryBalanceVC(nibName: "HistoryBalanceVC", bundle: nil)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @objc func refreshPressed(){
        let vc = HistoryBalanceVC(nibName: "HistoryBalanceVC", bundle: nil)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func calendarPressed(){
    }
    
    
    @IBAction func profilePressed(_ sender: Any) {
        let vc = ProfileVC(nibName: "ProfileVC", bundle: nil)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func filialBtnPressed(_ sender: Any) {
        let vc = SelectBranchAlertVC(nibName: "SelectBranchAlertVC", bundle: nil)
        vc.modalPresentationStyle = .custom
        vc.transitioningDelegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func languageBtnPressed(_ sender: Any) {

        let english : ((UIAlertAction) -> Void) = { (action) in
            
        }
        
        let uzbek : ((UIAlertAction) -> Void) = { (action) in
            
        }

        let russian : ((UIAlertAction) -> Void) = { (action) in
            
        }

                
        showSystemAlert(title: "Haqiqatdan ham chiqmoqchimisz", message: nil, alertType: .actionSheet, actionTitles: ["English", "Russian","Uzbek","Cancel"], style: [.default, .default, .default, .cancel], actions: [english,russian,uzbek,nil])
        
        

    }
    @IBAction func invitationBtnPressed(_ sender: Any) {
        let vc = OfferAndSuggestionsVC(nibName: "OfferAndSuggestionsVC", bundle: nil)
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func newsBtnPressed(_ sender: Any) {
        let vc = NewVC(nibName: "NewVC", bundle: nil)
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func aboutAppBtnPressed(_ sender: Any) {
    }
    @IBAction func organizationBtnPressed(_ sender: Any) {
    }
    @IBAction func shareBtnPressed(_ sender: Any) {
    }
    @IBAction func logOutBtnPressed(_ sender: Any) {
        
        let yes : ((UIAlertAction) -> Void) = { (action) in
            
        }
                
       showSystemAlert(title: "Haqiqatdan ham chiqmoqchimisz", message: nil, alertType: .alert, actionTitles: ["Ha", "Yo'q"], style: [.cancel, .default], actions: [yes,nil])
        
    }

    
    
}

//MARK: PresentStudentAlertDelegate
extension SettingVC: UIViewControllerTransitioningDelegate {
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        PresentationController(presentedViewController: presented, presenting: presenting)
    }
    
    
}


//MARK: NAVIGATION ITEMS
extension SettingVC{
    func setUpNavigation(){
        
        title = "Sozlamalar"
        
        let backButton = UIBarButtonItem()
        backButton.title = ""
        backButton.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        self.navigationController?.navigationBar.layer.cornerRadius = 12
        self.navigationController?.navigationBar.clipsToBounds = true
        self.navigationController?.navigationBar.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        let refreshBtn = UIBarButtonItem(image: UIImage(named: Icons.history_grey), style: .plain, target: self, action: #selector(refreshPressed))
        
        refreshBtn.tintColor = .white
        
        let calendarBtn = UIBarButtonItem(image: UIImage(named: Icons.bell), style: .plain, target: self, action: #selector(calendarPressed))
        
        navigationItem.rightBarButtonItems = [calendarBtn,refreshBtn]
        
        navigationController?.navigationBar.update(backroundColor: Colors.customBlue, titleColor: .white)
    }
}
