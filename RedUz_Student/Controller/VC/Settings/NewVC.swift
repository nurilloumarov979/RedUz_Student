//
//  NewVC.swift
//  RedUz_Student
//
//  Created by apple on 13/12/21.
//

import UIKit

class NewVC: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
            tableView.separatorStyle = .none
            tableView.register(NewTVC.nib(), forCellReuseIdentifier: NewTVC.identifier)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpNavigation()
    }

}


//MARK: TABLE VIEW
extension NewVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NewTVC.identifier, for: indexPath) as! NewTVC
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = NewsVC(nibName: "NewsVC", bundle: nil)
        present(vc, animated: true, completion: nil)
    }
    
}
//MARK: NAVIGATION ITEMS
extension NewVC{
    
    func setUpNavigation(){
        title = "Yangiliklar"
        let backButton = UIBarButtonItem()
        backButton.title = ""
        backButton.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        self.navigationController?.navigationBar.layer.cornerRadius = 12
        self.navigationController?.navigationBar.clipsToBounds = true
        self.navigationController?.navigationBar.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        navigationController?.navigationBar.update(backroundColor: Colors.customBlue, titleColor: .white)
    }
    
}


