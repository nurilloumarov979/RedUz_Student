//  ProfileVC.swift
//  RedUz_Student
//  Created by apple on 11/12/21.

import UIKit
import MobileCoreServices

class ProfileVC: UIViewController {

    let datePicker = UIDatePicker()

    @IBOutlet weak var profileUmg: UIImageView!
    @IBOutlet weak var saveBtn: UIButton!{
        didSet{
            saveBtn.layer.cornerRadius = saveBtn.frame.height/2
        }
    }
    
    @IBOutlet weak var nameTF: SloyTextField!
    @IBOutlet weak var surnameTF: SloyTextField!
    @IBOutlet weak var phoneTF: SloyTextField!
    @IBOutlet weak var birthdayTF: SloyTextField!
    @IBOutlet weak var fatherNameTF: SloyTextField!
    @IBOutlet weak var fatherPhoneTF: SloyTextField!
    @IBOutlet weak var motherNameTF: SloyTextField!
    @IBOutlet weak var motherPhoneTF: SloyTextField!
    @IBOutlet weak var universityTF: SloyTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpDatePicker()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpNavigation()
    }

    @IBAction func cameraPressed(_ sender: Any) {
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func calendarPressed(_ sender: Any) {
        birthdayTF.becomeFirstResponder()
    }
    
    @IBAction func savePressed(_ sender: Any) {
    }
    
}

//MARK: Date Picker
extension ProfileVC {
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        birthdayTF.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    func setUpDatePicker() {
        
        datePicker.datePickerMode = .date
        datePicker.preferredDatePickerStyle = .wheels
        //ToolBar
        birthdayTF.inputView = datePicker
        datePicker.timeZone = .autoupdatingCurrent
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        doneButton.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17, weight: .medium)], for: .normal)
        
        let bar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        bar.isTranslucent = true
        bar.sizeToFit()
        let space = UIBarButtonItem(systemItem: .flexibleSpace)
        birthdayTF.inputAccessoryView = bar
        bar.items = [space, doneButton]
    }
 
}


//MARK: Get Image
extension ProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
       func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           // Check for the media type
             let mediaType = info[UIImagePickerController.InfoKey.mediaType] as! CFString
             switch mediaType {
             case kUTTypeImage:
               // Handle image selection result
               let editedImage = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
               profileUmg.image = editedImage
               let originalImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
                 profileUmg.image = originalImage
             case kUTTypeMovie:
               // Handle video selection result
               let videoUrl = info[UIImagePickerController.InfoKey.mediaURL] as! URL
             default:
                 print("Something")
             }
             picker.dismiss(animated: true, completion: nil)
       }
       
       func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
           dismiss(animated: true, completion: nil)
           print("CACNELLED")
       }
       
   }

//MARK: NAVIGATION ITEMS
extension ProfileVC{
    func setUpNavigation(){
        
        title = "Talaba Malumoti"
        
        let backButton = UIBarButtonItem()
        backButton.title = ""
        backButton.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        self.navigationController?.navigationBar.layer.cornerRadius = 12
        self.navigationController?.navigationBar.clipsToBounds = true
        self.navigationController?.navigationBar.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        
        navigationController?.navigationBar.update(backroundColor: Colors.customBlue, titleColor: .white)
    }
}

