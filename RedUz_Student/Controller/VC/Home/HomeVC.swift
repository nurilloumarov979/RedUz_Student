//  HomeVC.swift
//  RedUz_Student
//  Created by apple on 07/12/21.


import UIKit

class HomeVC: UIViewController {
    
    var datePicker = DatePickerDialog()
    
    var didPersonPressed = true
    let feedbackGenerator = UIImpactFeedbackGenerator(style: .light)

    @IBOutlet weak var personBtnStack: UIStackView!
    @IBOutlet weak var personBtn: UIButton!
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
            tableView.sectionHeaderTopPadding = 0
            tableView.contentInset  = UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0)
            tableView.register(MainTVC.nib(), forCellReuseIdentifier: MainTVC.identifier)
            tableView.separatorStyle = .none
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigation()
        self.personBtnStack.isHidden = true

    }
    
    
    @IBAction func studentPressed(_ sender: Any) {
    }
    
    @IBAction func callBtnPressed(_ sender: Any) {
    }
    
    @IBAction func smsBtnPressed(_ sender: Any) {
    }

    @IBAction func personBtnPressed(_ sender: Any) {
        if didPersonPressed {
            
            self.personBtn.setImage(UIImage(named: Icons.xVector), for: .normal)
            self.personBtnStack.alpha = 1
            self.personBtnStack.isHidden = false

        } else {
            self.personBtn.setImage(UIImage(named: Icons.person), for: .normal)
            self.personBtnStack.alpha = 0
            self.personBtnStack.isHidden = true
        }
        didPersonPressed = !didPersonPressed
        feedbackGenerator.impactOccurred()

    }

    @objc func notificationPressed(){
        let vc = NewVC(nibName: "NewVC", bundle: nil)
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @objc func calendarPressed(){
        datePickerTapped()
    }
    
}

//MARK: TABLE VIEW
extension HomeVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MainTVC.identifier, for: indexPath) as! MainTVC
        cell.updateCell(index: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = AttendanceVC(nibName: "AttendanceVC", bundle: nil)
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
//MARK: Date Picker Confugrate
extension HomeVC {
    
    func datePickerTapped() {
        let currentDate = Date()
        var dateComponents = DateComponents()
        dateComponents.month = -3
        let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)

        datePicker.show("DatePickerDialog",
                        doneButtonTitle: "Done",
                        cancelButtonTitle: "Cancel",
                        minimumDate: threeMonthAgo,
                        maximumDate: currentDate,
                        datePickerMode: .date) { (date) in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy"
                print(formatter.string(from: dt),333)
            }
        }
    }
    

}
//MARK: NAVIGATION ITEMS
extension HomeVC{
    
    func setUpNavigation(){
        
        navigationController?.navigationBar.update(backroundColor: Colors.customBlue, titleColor: .white)
        title = "Noyabr 16"
        navigationController?.navigationBar.barTintColor = .red
        self.navigationController?.navigationBar.layer.cornerRadius = 12
        self.navigationController?.navigationBar.clipsToBounds = true
        
        self.navigationController?.navigationBar.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]

        let notificationBtn = UIBarButtonItem(image: UIImage(named: Icons.bell), style: .plain, target: self, action: #selector(notificationPressed))
        notificationBtn.tintColor = .white
        let calendarBtn = UIBarButtonItem(image: UIImage(named: Icons.calendar_month), style: .plain, target: self, action: #selector(calendarPressed))
        
        navigationItem.rightBarButtonItems = [calendarBtn,notificationBtn]
        
    }

}
