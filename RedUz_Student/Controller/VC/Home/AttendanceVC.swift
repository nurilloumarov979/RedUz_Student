//
//  AttendanceVC.swift
//  RedUz_Student
//
//  Created by apple on 09/12/21.
//

import UIKit

class AttendanceVC: UIViewController {
    
    var didPersonPressed = true
    
    let feedbackGenerator = UIImpactFeedbackGenerator(style: .light)

    @IBOutlet weak var personBtnStack: UIStackView!
    
    @IBOutlet weak var personBtn: UIButton!

    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.register(AttendenceCVC.nib(), forCellWithReuseIdentifier: AttendenceCVC.identifier)
            collectionView.register(AttendCVC.nib(), forCellWithReuseIdentifier: AttendCVC.identifier)
            collectionView.contentInset = UIEdgeInsets(top: 6, left: 16, bottom: 16, right: 16)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigation()
        personBtnStack.isHidden = true
    }
    
    @objc func calendarPressed(){}
    
    @IBAction func studentPressed(_ sender: Any) {
    }
    
    @IBAction func callBtnPressed(_ sender: Any) {
    }
    
    @IBAction func smsBtnPressed(_ sender: Any) {
    }

    @IBAction func personBtnPressed(_ sender: Any) {
        if didPersonPressed {
                self.personBtn.setImage(UIImage(named: Icons.xVector), for: .normal)
                self.personBtnStack.isHidden = false
        } else {
            
            self.personBtn.setImage(UIImage(named: Icons.person), for: .normal)
            self.personBtnStack.isHidden = true
        }
        didPersonPressed = !didPersonPressed
        feedbackGenerator.impactOccurred()
    }
    

}


//MARK: COLLECTION VIEW
extension AttendanceVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AttendCVC.identifier, for: indexPath) as! AttendCVC
            
            return cell
            
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AttendenceCVC.identifier, for: indexPath) as! AttendenceCVC
            return cell
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0{
           return CGSize(width: collectionView.frame.width-32, height: 95)
        }else{
            if UIScreen.main.bounds.height >= 650{
                return  CGSize(width: (collectionView.frame.width-56)/3, height: 70)
            }else{
                return  CGSize(width: (collectionView.frame.width-56)/3, height: 60)
            }
        }
    }
    
}

//MARK: NAVIGATION ITEMS
extension AttendanceVC{
    func setUpNavigation(){
        title = "Noyabr 16"
        let backButton = UIBarButtonItem()
        backButton.title = ""
        backButton.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        self.navigationController?.navigationBar.layer.cornerRadius = 12
        self.navigationController?.navigationBar.clipsToBounds = true
        self.navigationController?.navigationBar.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        let calendarBtn = UIBarButtonItem(image: UIImage(named: Icons.bell), style: .plain, target: self, action: #selector(calendarPressed))
        navigationItem.rightBarButtonItems = [calendarBtn]
        navigationController?.navigationBar.update(backroundColor: Colors.customBlue, titleColor: .white)
    }
}
