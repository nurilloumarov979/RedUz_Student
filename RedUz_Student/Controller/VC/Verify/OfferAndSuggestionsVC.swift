//
//  OfferAndSuggestionsVC.swift
//  RedUzTeacher
//
//  Created by Kuziboev Siddikjon on 12/13/21.
//

import UIKit

class OfferAndSuggestionsVC: UIViewController {
    
    let segmentControl = UISegmentedControl (items: ["Taklif","Shikoyat"])
    
    @IBOutlet weak var sendBtn: UIButton!{
        didSet {
            sendBtn.addShadow(offset: CGSize(width: 0, height: 0), color: #colorLiteral(red: 0.2392156863, green: 0.4078431373, blue: 1, alpha: 0.12), radius: 4, opacity: 0.9)
        }
    }
    
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var lblQuestion: UILabel!
    
    var theme: String = "Mavzu: "
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTextView()
        setUpSegment()
    }
    
    func setUpSegment(){
        segmentControl.backgroundColor = Colors.customBackgraund
        
        let xPostion:CGFloat = 10
        let yPostion:CGFloat = 150
        let elementWidth:CGFloat = 200
        let elementHeight:CGFloat = 24
        segmentControl.frame = CGRect(x: xPostion, y: yPostion, width: elementWidth, height: elementHeight)
        navigationItem.titleView = segmentControl
    }

    
    @IBAction func sendBtnPressed(_ sender: Any) {
    }

    
}




//MARK: TextViewDelegate
extension OfferAndSuggestionsVC: UITextViewDelegate {
    
    func setUpTextView() {
        textView.delegate = self
        textView.text = theme
        textView.textColor = UIColor.lightGray
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = theme
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        if textView.text.isEmpty || textView.text.count < theme.count {
            textView.text = theme
            textView.textColor = UIColor.lightGray
        }else {
            textView.textColor = UIColor.black
            
        }
    }
}

