//  SplashVC.swift
//  Red_Uz_Student
//  Created by apple on 07/12/21.

import UIKit

class SplashVC: UIViewController {

    @IBOutlet weak var logoImage: UIImageView!
    
    @IBOutlet weak var firstView: UIView!
    
    @IBOutlet weak var secondView: UIView!
    
    @IBOutlet weak var bigContainerView: UIView!{
        didSet{
            bigContainerView.layer.cornerRadius = bigContainerView.frame.height/2
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bigContainerView.transform = .init(scaleX: 1500, y: 1500)
        animate()

    }

    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        firstView.layer.cornerRadius = firstView.frame.height/2
        secondView.layer.cornerRadius = secondView.frame.height/2
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
    }
    
    func animate(){
        
        let vc = FirstLanguageVC(nibName: "FirstLanguageVC", bundle: nil)
        let nav = UINavigationController(rootViewController: vc)
        
        nav.modalPresentationStyle = .fullScreen
        
        if UIScreen.main.bounds.height > 700 {
            
            UIView.animate(withDuration: 2) {[self] in
                bigContainerView.transform = .identity
                bigContainerView.layer.cornerRadius = bigContainerView.frame.height/2

            }completion: { isTrue in
                
                self.bigContainerView.backgroundColor = .white
                
                UIView.animate(withDuration: 2.2) {
                    self.logoImage.transform = .init(scaleX: 1.6, y: 1.6)
                } completion: { isTrue in
                    self.present(nav, animated: false, completion: nil)
                }
                
                UIView.animate(withDuration: 2) {
                    self.secondView.transform = .init(translationX: -self.view.frame.width/2+50, y: -self.view.frame.height/2+50)
                } completion: { isTrue in
                }
                
                UIView.animate(withDuration: 2) {
                    self.firstView.transform = .init(translationX: self.view.frame.width/2-95, y: self.view.frame.height/2-95)
                } completion: { isTrue in
                    
                }
            }

        }else{
            UIView.animate(withDuration: 2) {[self] in
                bigContainerView.transform = .identity
                bigContainerView.layer.cornerRadius = bigContainerView.frame.height/2

            } completion: { isTrue in
                self.bigContainerView.backgroundColor = .white
                
                UIView.animate(withDuration: 2.2) {
                    self.logoImage.transform = .init(scaleX: 1.6, y: 1.6)
                } completion: { isTrue in
                    
                    self.present(nav, animated: true, completion: nil)
                    
                }
                
                UIView.animate(withDuration: 2) {
                    self.secondView.transform = .init(translationX: -self.view.frame.width/2+60, y: -self.view.frame.height/2+60)
                } completion: { isTrue in
                }
                
                UIView.animate(withDuration: 2) {
                    self.firstView.transform = .init(translationX: self.view.frame.width/2-60, y: self.view.frame.height/2-60)
                } completion: { isTrue in
                    
                }
            }

        }

    }

}
