//
//  PhoneNumberVC.swift
//  RedUz_Student
//
//  Created by apple on 08/12/21.
//

import UIKit

class PhoneNumberVC: UIViewController {

    
    @IBOutlet weak var enterBtn: UIButton!{
        didSet{
            enterBtn.layer.cornerRadius = enterBtn.layer.frame.height/2
        }
    }
    @IBOutlet weak var phoneNumberTF: UITextField!{
        didSet{
            phoneNumberTF.layer.cornerRadius = phoneNumberTF.frame.height/2
            phoneNumberTF.layer.shadowColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            phoneNumberTF.layer.shadowRadius = 2
            phoneNumberTF.layer.shadowOffset = CGSize(width: 1, height: 1)
            phoneNumberTF.layer.shadowOpacity = 0.2
            phoneNumberTF.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
            phoneNumberTF.leftViewMode = .always
            phoneNumberTF.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
            phoneNumberTF.rightViewMode = .always
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func enterPressed(_ sender: Any) {
        let vc = OTPVC(nibName: "OTPVC", bundle: nil)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
