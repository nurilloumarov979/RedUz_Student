
//  FirstLanguageVC.swift
//  RedUz_Student
//  Created by apple on 07/12/21.

import UIKit

class FirstLanguageVC: UIViewController {

    @IBOutlet weak var enterBtn: UIButton!{
        didSet{
            enterBtn.layer.cornerRadius = enterBtn.frame.height/2
        }
    }
    @IBOutlet var languageViews: [UIView]!{
        didSet{
            for v in languageViews{
                v.layer.cornerRadius = v.frame.height/2
                v.layer.shadowColor = UIColor.systemGray.cgColor
                v.layer.shadowOffset = CGSize(width: 1, height: 1)
                v.layer.shadowRadius = 2
                v.layer.shadowOpacity = 0.3
            }
        }
    }
    
    
    @IBOutlet var languageBtns: [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        enterBtn.isEnabled = false
    }

    
    @IBAction func languageBtnsPressed(_ sender: UIButton) {
        enterBtn.backgroundColor = Colors.customBlue
        enterBtn.isEnabled = true
    }
    
    @IBAction func enterPressed(_ sender: Any) {
        
        
        let vc = PhoneNumberVC(nibName: "PhoneNumberVC", bundle: nil)
     navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}
