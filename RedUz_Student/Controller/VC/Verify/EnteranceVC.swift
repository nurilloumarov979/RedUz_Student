//
//  EnteranceVC.swift
//  RedUz_Student
//
//  Created by apple on 09/12/21.
//

import UIKit

class EnteranceVC: UIViewController {

    @IBOutlet weak var phoneTF: UITextField!{
        didSet{
            phoneTF.layer.cornerRadius = phoneTF.frame.height/2
            phoneTF.layer.shadowColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            phoneTF.layer.shadowOffset = CGSize(width: -1, height: -1)
            phoneTF.layer.shadowRadius = 2
            phoneTF.layer.shadowOpacity = 0.3
            phoneTF.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
            parolTF.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
            phoneTF.rightViewMode = .always
            phoneTF.leftViewMode = .always

        }
    }
    
    @IBOutlet weak var parolTF: UITextField!{
        didSet{
            parolTF.layer.cornerRadius = parolTF.frame.height/2
            parolTF.layer.shadowColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            parolTF.layer.shadowOffset = CGSize(width: -1, height: -1)
            parolTF.layer.shadowRadius = 2
            parolTF.layer.shadowOpacity = 0.3
            parolTF.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
            parolTF.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
            parolTF.rightViewMode = .always
            parolTF.leftViewMode = .always
        }
    }
    
    @IBOutlet weak var enterBtn: UIButton!{
        didSet{
            enterBtn.layer.cornerRadius = enterBtn.frame.height/2
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func enterPressed(_ sender: Any) {
        
        
                if let window = UIApplication.shared.keyWindow {
                    let vc = MainTab(nibName: "MainTab", bundle: nil)
                    window.rootViewController = vc
                    window.makeKeyAndVisible()
                }

    }
    
}
