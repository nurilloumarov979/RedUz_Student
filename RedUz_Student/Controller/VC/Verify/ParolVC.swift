//
//  ParolVC.swift
//  RedUz_Student
//
//  Created by apple on 09/12/21.
//

import UIKit

class ParolVC: UIViewController {

    @IBOutlet weak var parolTF: UITextField!{
        didSet{
            parolTF.layer.cornerRadius = parolTF.frame.height/2
            parolTF.layer.shadowColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            parolTF.layer.shadowOffset = CGSize(width: -1, height: -1)
            parolTF.layer.shadowRadius = 2
            parolTF.layer.shadowOpacity = 0.3
            parolTF.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
            parolTF.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
            parolTF.rightViewMode = .always
            parolTF.leftViewMode = .always
        }
    }
    
    
    @IBOutlet weak var returnParolTF: UITextField!{
        didSet{
            returnParolTF.layer.cornerRadius = returnParolTF.frame.height/2
            returnParolTF.layer.shadowColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            returnParolTF.layer.shadowOffset = CGSize(width: -1, height: -1)
            returnParolTF.layer.shadowRadius = 2
            returnParolTF.layer.shadowOpacity = 0.3
            returnParolTF.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
            returnParolTF.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
            returnParolTF.rightViewMode = .always
            returnParolTF.leftViewMode = .always
        }
    }
    
    @IBOutlet weak var parolBtn: UIButton!{
        didSet{
            parolBtn.layer.cornerRadius = parolBtn.frame.height/2
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()


    }

    
    
    @IBAction func parolPressed(_ sender: Any) {
        
        let vc = EnteranceVC(nibName: "EnteranceVC", bundle: nil)
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
