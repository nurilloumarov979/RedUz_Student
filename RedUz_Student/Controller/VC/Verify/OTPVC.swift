//
//  OTPVC.swift
//  RedUz_Student
//
//  Created by apple on 08/12/21.
//

import UIKit

class OTPVC: UIViewController {

    @IBOutlet weak var otpView: OTPFieldView!
    @IBOutlet weak var verifyBtn: UIButton!{
        didSet{
            verifyBtn.layer.cornerRadius = verifyBtn.frame.height/2
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupOtpView()
    }


    @IBAction func verifyPressed(_ sender: Any) {
        let vc = ParolVC(nibName: "ParolVC", bundle: nil)
        navigationController?.pushViewController(vc, animated: true)
    }
    
}


extension OTPVC:OTPFieldViewDelegate{
    
    
    func setupOtpView(){
        self.otpView.fieldsCount = 4
        self.otpView.fieldBorderWidth = 1.5
     self.otpView.defaultBorderColor = Colors.customGray
        self.otpView.filledBorderColor = Colors.customBlue
        self.otpView.cursorColor = Colors.customBlue
        self.otpView.displayType = .roundedCorner
        self.otpView.fieldSize = 48
        self.otpView.otpInputType = .numeric
        self.otpView.separatorSpace = 16
        self.otpView.shouldAllowIntermediateEditing = false
        self.otpView.delegate = self
        self.otpView.initializeUI()
    }
    
    

    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp: String) {
        
    }
    
    func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
        return true
    }
    
    
}

