
//  AttendenceCVC.swift
//  RedUz_Student
//  Created by apple on 10/12/21.

import UIKit


class AttendenceCVC: UICollectionViewCell {
    
    static let identifier = "AttendenceCVC"
    static func nib()->UINib{return UINib(nibName: identifier, bundle: nil)}
    
    @IBOutlet weak var dateLbl: UILabel!{
        didSet{
            if UIScreen.main.bounds.height >= 650{
                dateLbl.font = .systemFont(ofSize: 18)
            }else{
                dateLbl.font = .systemFont(ofSize: 14)
            }
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
}
