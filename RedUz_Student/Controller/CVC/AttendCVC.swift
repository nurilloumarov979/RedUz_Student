//
//  AttendCVC.swift
//  RedUz_Student
//
//  Created by apple on 10/12/21.
//

import UIKit

class AttendCVC: UICollectionViewCell {
    static let identifier = "AttendCVC"
    static func nib()->UINib{return UINib(nibName: identifier, bundle: nil)}
    @IBOutlet weak var containerView: UIView!{
        didSet{
            containerView.addShadow(offset: CGSize(width: 1, height: 1), color: Colors.customGray.cgColor, radius: 2, opacity: 0.2)
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
